package info.textgrid.lab.iiif.viewer.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class ManifestGeneratorPerspective implements IPerspectiveFactory{

	@Override
	public void createInitialLayout(IPageLayout layout) {
		// TODO Auto-generated method stub
		
    	String editorArea = layout.getEditorArea();
    	
    	IFolderLayout topLeft = 
    		layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.3, editorArea);//$NON-NLS-1$
       	topLeft.addView("info.textgrid.lab.navigator.view");
 
    	IFolderLayout topRight= 
    		layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.4, editorArea);//$NON-NLS-1$
       	topRight.addView("info.textgrid.lab.iiif.viewer.views.ManifestGeneratorView");
    	
    	layout.setEditorAreaVisible(false); 
	}

}
