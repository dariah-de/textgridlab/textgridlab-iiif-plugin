package info.textgrid.lab.iiif.viewer.tester;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;

public class IsViewableObject extends PropertyTester {

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		if (receiver != null && receiver instanceof TGObjectReference) {
			return isOK(AdapterUtils.getAdapter(receiver, TextGridObject.class));
		}

		return true;
	}

	private boolean isOK(TextGridObject tgo) {
		
		String contentTypeId = "";
		try {
			contentTypeId = tgo.getContentTypeID();
			System.out.println(tgo.getContentTypeID());
			if (   contentTypeId.contains("tg.aggregation")
					|| contentTypeId.contains("text/linkeditorlinkedfile")) {
				return true;
			}
		} catch (CoreException e) {
			info.textgrid.lab.iiif.viewer.Activator
					.handleWarning(e,
							"Couldn't get the content type id of the selected textgrid object");
		}

		return false;
	}
}

