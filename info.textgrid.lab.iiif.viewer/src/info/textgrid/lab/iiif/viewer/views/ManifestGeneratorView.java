package info.textgrid.lab.iiif.viewer.views;


import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.iiif.viewer.Activator;
import info.textgrid.lab.iiif.viewer.preferences.PluginPreferencePage;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;


public class ManifestGeneratorView extends ViewPart {

	private Browser spBrowser;
	private boolean browserReady = false;
	Queue<String> jsExecutions = new LinkedList<String>();

	public void createPartControl(Composite parent) {

    	try {
    		spBrowser = new Browser(parent, SWT.NONE);
    	} catch (SWTError e) {
			System.out.println("Could not instantiate Browserview " + e.getMessage());
			return;
		}

    	String url = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.manifest_generator_url_id, "", null);

    	if(url.equals("")) {
    		spBrowser.setText("No IIIF Manifest-Generator URL, please set one with Window->Preferences->IIIF Viewer");
    		return;
    	}

    	spBrowser.setUrl(url);
		spBrowser.addProgressListener(new ProgressListener() {
			public void changed(ProgressEvent arg0) {}
			public void completed(ProgressEvent arg0) {
				String sid = RBACSession.getInstance().getSID(false);
				spBrowser.execute("setSid('"+sid+"')");
				browserReady = true;
				Iterator<String> it = jsExecutions.iterator();
				while(it.hasNext()) {
					String command = it.next();
					spBrowser.execute(command);
					it.remove();
				}
			}
		});

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		//viewer.getControl().setFocus();
	}

	public void addTextGridObject(String uri, String title, String contentType) {
		String command = "addTGObject('"+uri+"', '"+title + " ("+uri+")"+"', '"+contentType+"')";
		if(browserReady) {
			spBrowser.execute(command);
		} else {
			jsExecutions.add(command);
		}
	}
}

