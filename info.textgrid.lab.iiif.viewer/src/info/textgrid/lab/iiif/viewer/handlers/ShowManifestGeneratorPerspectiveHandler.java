package info.textgrid.lab.iiif.viewer.handlers;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.iiif.viewer.Activator;
import info.textgrid.lab.iiif.viewer.views.ManifestGeneratorView;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;

public class ShowManifestGeneratorPerspectiveHandler  extends AbstractHandler implements IHandler{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			wb.getActiveWorkbenchWindow().getActivePage().setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"info.textgrid.lab.iiif.viewer.perspectives.ManifestGeneratorPerspective"));
			wb.getActiveWorkbenchWindow().getActivePage().resetPerspective();

			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.iiif.viewer.perspectives.ManifestGeneratorPerspective",
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
			
			// test if command was called from navigator
			if(event.getCommand().getId().equals("info.textgrid.lab.iiif.viewer.openSelected")) {

				ISelection selection = HandlerUtil.getCurrentSelection(event);
				if (selection instanceof IStructuredSelection) {

					for (Object obj : ((IStructuredSelection) selection).toList()){
						if (obj instanceof IAdaptable) {
							TextGridObject tgo = (TextGridObject) ((IAdaptable) obj)
									.getAdapter(TextGridObject.class);
							
							String title = tgo.getTitle();
							String uri = tgo.getURI().toString();
							String contentType = tgo.getMetadata().getGeneric().getProvided().getFormat();
							
							((ManifestGeneratorView)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
								.findView("info.textgrid.lab.iiif.viewer.views.ManifestGeneratorView")).addTextGridObject(uri, title, contentType);
							
						}
					}
				}
			}
			
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Could not open IIIF Viewer Perspective!", e);
			Activator.getDefault().getLog().log(status);
		} catch (CoreException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Error !", e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}

}

