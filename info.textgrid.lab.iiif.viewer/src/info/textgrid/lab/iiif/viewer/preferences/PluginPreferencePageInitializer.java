package info.textgrid.lab.iiif.viewer.preferences;

import info.textgrid.lab.iiif.viewer.Activator;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

public class PluginPreferencePageInitializer extends AbstractPreferenceInitializer{

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		store.setDefault(PluginPreferencePage.manifest_generator_url_id, "http://localhost:8080/exist/apps/iiif/generator.html");

	}

}
