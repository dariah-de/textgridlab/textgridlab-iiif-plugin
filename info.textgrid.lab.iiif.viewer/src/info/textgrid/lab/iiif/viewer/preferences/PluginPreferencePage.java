package info.textgrid.lab.iiif.viewer.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import info.textgrid.lab.iiif.viewer.Activator;

public class PluginPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage{

	public static String manifest_generator_url_id = "manifest_generator_url";
	public static String trivia_url_id = "trivia_url";
	
	@Override
	protected void createFieldEditors() {

		addField(new StringFieldEditor(manifest_generator_url_id, "IIIF Manifest Service URL", getFieldEditorParent()));
		addField(new StringFieldEditor(trivia_url_id, "Trivia URL", getFieldEditorParent()));
		
	}

	@Override
	public void init(IWorkbench workbench) {

		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		
	}

}
