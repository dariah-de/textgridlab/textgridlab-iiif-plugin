package info.textgrid.lab.iiif.trivia.views;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.iiif.viewer.Activator;
import info.textgrid.lab.iiif.viewer.preferences.PluginPreferencePage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.ViewPart;


public class TriviaEditor extends EditorPart {

	private Browser spBrowser;
	private boolean browserReady = false;
	Queue<String> jsExecutions = new LinkedList<String>();
	
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		// TODO Auto-generated method stub

		try {
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.iiif.trivia.perspectives.TriviaPerspective",
					PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			Activator.handleError(e, "error opening trivia perspective");
		}
		
		TextGridObject tgo = (TextGridObject) input.getAdapter(TextGridObject.class);
		if (tgo != null) {
			TriviaInput tInput = new TriviaInput(tgo);
			setSite(site);
			setInput(tInput);
			
			try {
				String title = tgo.getTitle();
				String uri = tgo.getURI().toString();
				String contentType = tgo.getMetadata().getGeneric().getProvided().getFormat();	
				
				this.setPartName(title);				
				addTextGridObject(uri, title, contentType);
			} catch (CoreException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					    "Error !", e);
				Activator.getDefault().getLog().log(status);
			}

		}
		
	}

	public void createPartControl(Composite parent) {

    	try {
    		spBrowser = new Browser(parent, SWT.NONE);
    	} catch (SWTError e) {
			System.out.println("Could not instantiate Browserview " + e.getMessage());
			return;
		}

    	String url = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, PluginPreferencePage.trivia_url_id, "", null);

    	if(url.equals("")) {
    		spBrowser.setText("No Trivia URL, please set one with Window->Preferences->IIIF Viewer");
    		return;
    	}

    	spBrowser.setUrl(url);
		spBrowser.addProgressListener(new ProgressListener() {
			public void changed(ProgressEvent arg0) {}
			public void completed(ProgressEvent arg0) {
				String sid = RBACSession.getInstance().getSID(false);
				spBrowser.execute("setSid('"+sid+"')");
				browserReady = true;
				Iterator<String> it = jsExecutions.iterator();
				while(it.hasNext()) {
					String command = it.next();
					spBrowser.execute(command);
					it.remove();
				}
			}
		});

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		//viewer.getControl().setFocus();
	}

	public void addTextGridObject(String uri, String title, String contentType) {
		String command = "addTGObject('"+uri+"', '"+title + " ("+uri+")"+"', '"+contentType+"')";
		if(browserReady) {
			spBrowser.execute(command);
		} else {
			jsExecutions.add(command);
		}
	}

	@Override
	public void doSave(IProgressMonitor arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}
}

